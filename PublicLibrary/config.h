#ifndef CONFIG_H
#define CONFIG_H

#include <QObject>


class config : public QObject
{
    Q_OBJECT


public:
    int delayVideo;
    QString videoPath;

    void  jsonFile();

    explicit config(QObject *parent = nullptr);
signals:

};

#endif // CONFIG_H
