#include "config.h"



#include <QJsonDocument>
#include <QJsonParseError>
#include <QFile>
#include <QJsonObject>
#include <QDebug>
#include <QJsonArray>




config::config(QObject *parent)
    : QObject{parent}
{

    jsonFile();
}


void config::jsonFile()
{
    QFile file("./configInfo.json");
    if(!file.open(QIODevice::ReadOnly))
    {

    QVariantHash Data;
    Data.insert("Mp4_Path", "./1.mp4");
    Data.insert("Delay_time", 2000);

    QJsonObject rootObj = QJsonObject::fromVariantHash(Data);
    QJsonDocument document;
    document.setObject(rootObj);

    QByteArray byte_array = document.toJson(QJsonDocument::Compact);
    QString json_str(byte_array);
    //根据实际填写路径
    QFile file("./configInfo.json");

    if (!file.open(QIODevice::ReadWrite | QIODevice::Text))
    {
        qDebug() << "file error!";
    }
    QTextStream in(&file);
    in << json_str;
    file.close();   // 关闭file
    }

    QJsonParseError jerr;
    QJsonDocument doc = QJsonDocument::fromJson(file.readAll(), &jerr);
    QJsonObject jroot = doc.object();


        delayVideo = jroot["Delay_time"].toInt();
        videoPath = jroot["Mp4_Path"].toString();


    qInfo()<<"shuchu"<<delayVideo<<"  "<<videoPath;

}
