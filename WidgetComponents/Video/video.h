#ifndef VIDEO_H
#define VIDEO_H

#include <QWidget>
#include <QtMultimedia>
#include <QVideoWidget>
#include <Windows.h>
#include <QTime>

#include <QDebug>
#include "../../PublicLibrary/config.h"

namespace Ui {
class Video;
}

class Video : public QWidget
{
    Q_OBJECT

public:
    explicit Video(QWidget *parent = nullptr);
    ~Video();


    bool isLoop;
    QTimer *tim;
    void MaxWindowFun();
    void MinWindowFun();
    config*con=nullptr;

public  slots:
   void SmediaStatusChanged(QMediaPlayer::MediaStatus status);

private:

    QVideoWidget    *       m_pPlayerWidget;
    QMediaPlayer    *       m_pPlayer;
    Ui::Video *ui;
    HHOOK keyboardHook;

public slots:
    void key_up();
    void key_down();


};



class keymonitor : public QObject
{
    Q_OBJECT
private:
    static keymonitor*m_keymonitor;


private:
   keymonitor()
   {

   }
public:
   ~keymonitor()
   {

   };
   static keymonitor*getinstance()
   {
       if(m_keymonitor==nullptr)
       {
           m_keymonitor=new keymonitor();
       }
       return m_keymonitor;
   };
   static LRESULT LowLevelKeyboardProc(int nCode, WPARAM wParam, LPARAM lParam)
   {
       KBDLLHOOKSTRUCT* ks = (KBDLLHOOKSTRUCT*)lParam;
       if (ks->flags == 128 || ks->flags == 129 /*|| ks->flags == 0*/)
       {
           if (ks->vkCode == 38)
           {
                emit keymonitor::getinstance()->winkey_up();
           }
           else if (ks->vkCode == 40)
           {
                emit keymonitor::getinstance()->winkey_down();
           }
       }
       return CallNextHookEx(NULL, nCode, wParam, lParam);
   };
signals:
    void winkey_up();
    void winkey_down();


};
#endif // VIDEO_H





