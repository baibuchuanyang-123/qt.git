#include "video.h"
#include "ui_video.h"


keymonitor* keymonitor::m_keymonitor = nullptr;

Video::Video(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Video)
{
    con=new config;
    ui->setupUi(this);


    setFocusPolicy(Qt::ClickFocus);
    isLoop=false;

    m_pPlayer = new QMediaPlayer;

    m_pPlayer->setSource(QUrl(con->videoPath));
    qInfo()<<"kdkdk"<<con->videoPath;

    m_pPlayerWidget = new QVideoWidget;

    m_pPlayer->setVideoOutput(m_pPlayerWidget);

    m_pPlayer->setAudioOutput(new QAudioOutput());


    ui->verticalLayout->addWidget(m_pPlayerWidget);

    m_pPlayerWidget->setAutoFillBackground(true);
    QPalette qplte;
    qplte.setColor(QPalette::Window, QColor(0,0,0));
    m_pPlayerWidget->setPalette(qplte);

    // connect(keymonitor::getinstance(),SIGNAL(windowMinMax()), this, SLOT(test()));

    //播放
    connect(keymonitor::getinstance(),SIGNAL(winkey_up()), this, SLOT(key_up()));
    //停止
    connect(keymonitor::getinstance(),SIGNAL(winkey_down()), this, SLOT(key_down()));

    connect(m_pPlayer,SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)),this,SLOT(SmediaStatusChanged(QMediaPlayer::MediaStatus)));

    m_pPlayer->play();

    //暂停
    //connect(ui->BtnLoad,SIGNAL(clicked()),m_pPlayer,SLOT(pause()));


    keyboardHook = 0;
    // LaunchThirdExe::Get()->StartThirdExe(const_cast<char*>("E:\\VsCode\\Install\\Microsoft VS Code\\Code.exe"));
    // LaunchThirdExe::Get()->SwitchWindowFull();

    keyboardHook = SetWindowsHookEx(
        WH_KEYBOARD_LL,
        &keymonitor::LowLevelKeyboardProc,
        GetModuleHandleA(NULL),
        NULL
    );

    tim = new QTimer();



}



Video::~Video()
{
    delete m_pPlayer;
    delete m_pPlayerWidget;
    delete ui;
}

void Video::MaxWindowFun()
{
    this->parentWidget()->showFullScreen();
    this->activateWindow();
    this->setWindowState((this->windowState() & ~Qt::WindowMinimized) | Qt::WindowActive);
    m_pPlayer->play();

}

void Video::MinWindowFun()
{
    m_pPlayer->stop();

    this->parentWidget()->showMinimized();
}

void Video::SmediaStatusChanged(QMediaPlayer::MediaStatus status)
{

    if(QMediaPlayer::EndOfMedia==status)
    {

        if(isLoop)
        {
            MinWindowFun();
            tim->setInterval(con->delayVideo);
            connect(tim,&QTimer::timeout,this,[=](){
                MaxWindowFun();
                tim->stop();
            });
            tim->start();
        }
        else
        {
            MinWindowFun();
        }


    }
}

void Video::key_up()
{

    if(this->parentWidget()->isMinimized())
    {
        MaxWindowFun();
        isLoop=false;

    }
    else if(this->parentWidget()->isFullScreen())
    {
        MinWindowFun();


    }

}

void Video::key_down()
{

  MaxWindowFun();
  isLoop=true;

}
