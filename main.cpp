#include "mainwindow.h"

#include <QApplication>

#include <QSharedMemory>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QSharedMemory shared_memory;
    shared_memory.setKey(QString("666666"));//设置固定共享内存段的key值
    if(shared_memory.attach())   //尝试将进程附加到该共享内存段
    {
        return 0;
    }
    if(shared_memory.create(1)) //创建1byte的共享内存段
    {
        MainWindow w;
        w.activateWindow();
        w.setWindowState((w.windowState() & ~Qt::WindowMinimized) | Qt::WindowActive);

        w.show();

        //w.resize(1920, 1080);
        w.showMaximized();
        w.showFullScreen();
        return a.exec();
    }


}
